# heykube

A simple hello world service running in Kubernetes.

Gradle builds `hello.java` into `hello.jar`.
This jar is packaged into a Docker container, and the
the container is deployed to a Kubernets cluster.

Two replicated pods are created as indicated by replica field.

The pods are labeled `helloworld` and the deployment itself is named as `helloworld`
which can be fetched by running `kubectl get deployments` and further details
about the deployment can be fetched by running `kubect get deployment helloworld`,
or describe it by running `kubectl describe deployment helloworld`.

It is possible to scale the deployment by running `kubectl scale helloworld` 