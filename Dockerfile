FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD ./build/libs/hello*.jar /tmp/hello.jar
ENTRYPOINT ["java","-jar","/app.jar"]
